﻿using Laba5_3semestr.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Laba5_3semestr
{
    public partial class MainForm : Form
    {
        Data data = new Data();
        public MainForm()
        {
            InitializeComponent();
            listBox1.Items.Add(@"\bо[а-я]+");//все слова на о
            listBox1.Items.Add(@"[A-Z][^.!?]+\?");//ищет вопросительные предложения
            listBox1.Items.Add(@"\b(of|or)\b");//за один проход ищет of или or
            listBox1.Items.Add(@"\b(\S)\S*\1\b");//слова начинающиеся и заканчивающие на одну и ту же букву; (\1 – ссылка на первую круглую скобку)
            listBox1.Click += (s, e) =>
            {
                textBox1.Text = listBox1.Text;
                Find(s, e);
            };

        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"; // расширения
            dlg.FilterIndex = 1;
            DialogResult res = dlg.ShowDialog(); // показываем диалог и ждём ok или отмены
            if (res == DialogResult.OK) // если не нажали отмену
            {
                LoadFile(dlg.FileName);
            }
        }
        //чтобы избежать повтор кода в OpenFile и LoadConfig
        private void LoadFile(String name)
        {                                      
            data.ReadFromFile(name);
            richTextBox1.Text = data.Text;
            Text = data.FileName;
        }
        //нажатие на "Поиск"
        private void Find(object sender, EventArgs e)
        {
            data.Find(textBox1.Text);
            this.ShowMatch();
        }

        private void ShowMatch()
        {
            Match m = data.Match; // получить m из data, добавить using
            if (m != null && m.Success)
            {
                richTextBox1.SelectionBackColor = Color.White; // сброс подсветки
                richTextBox1.SelectionStart = m.Index;
                // начало - место, на котором
                // в строке найдено регулярное выражение
                richTextBox1.SelectionLength = m.Value.Length;
                // длина найденного фрагмента
                richTextBox1.ScrollToCaret(); // прокрутка на выделенное место
                richTextBox1.SelectionBackColor = Color.Yellow; // подсветка
                richTextBox2.Text = $"Найдено[{m.Index}]: ##{m.Value}##\n";
            }
            for (int i = 0; i < m.Groups.Count; i++)
            {
                richTextBox2.Text += String.Format("Groups[{0}]={1}\n", i, m.Groups[i]);
            }

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Find(sender, e);
                e.SuppressKeyPress = true; // дальше событие нажатие кнопки игнорируется
            }
        }
        // нажаие на "еще"
        private void NextMatch(object sender, EventArgs e)
        {
            data.NextMatch();
            ShowMatch();
        }
        #region
        private void SaveConfig(object sender, FormClosingEventArgs e)
        {
            Settings.Default.DefaultFileName = data.FileName;
            Settings.Default.Save();
        }

        private void LoadConfig(object sender, EventArgs e)
        {
            LoadFile(Settings.Default.DefaultFileName);
        }
        #endregion
        //ищет of или or
        private void oforToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int ofc, orc;
            data.GetOfOrStatistics(out ofc, out orc);//out - значит функция обязана их присвоить
            richTextBox2.Text = $" of: {ofc}, or: {orc}";
        }
        //возвращающую первые слова каждого предложения, заканчивающегося вопросительным знаком 
        private void findSentencesFirstWordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ISet<String> words = data.FindSentencesFirstWords();
            richTextBox2.Text = String.Join(", ", words);
        }
        //подсчёт первых символов слова
        private void FirstLetterCounts(object sender, EventArgs e)
        {
            new StatisticsForm(data.FirstLetterCounts(),("Word", "Count")).Show();
        }

        private void DopZadanie(object sender, EventArgs e)
        {
            new StatisticsForm(data.DopZadanie(),("Sentens", "Count")).Show();
        }
    }
}
