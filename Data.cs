﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Laba5_3semestr
{
    class Data
    {
        public String Text { get; private set; }
        public String FileName { get; private set; }
        public Match Match { get; private set; }

        internal void ReadFromFile(string fileName)
        {
            if (fileName == null || fileName.Length == 0)
                return;
            using (StreamReader sr = new StreamReader(fileName)) //создает обьект, который считывает конкретный текст
            {
                //ReadToEnd считывает весь файл вместе с символами перевода строк, 
                //по умолчанию используется кодировка UTF-8.
                // Мы также заменяем перевод строк из двух символов \r\n на односимвольный \n.
                Text = sr.ReadToEnd().Replace("\r", "");  //стандартный символ конца строки	                                                    
                FileName = fileName;//сохраняем, чтобы знать какой файл считывать
            }
        }

        public void Find(string re)
        {
            this.Match = Regex.Match(this.Text, re); // re – регулярное выражение, которое мы ищем (параметр метода Find)
        }

        public void NextMatch()
        {
            Match = Match?.NextMatch();
        }

        internal void GetOfOrStatistics(out int ofc, out int orc)
        {
            ofc = 0; orc = 0;
            foreach (Match m in Regex.Matches(this.Text, @"\b(of|or)\b"))
            {
                if (m.Value == "of") ofc++; else orc++;
            }
        }
        //возвращающую первые слова каждого предложения, заканчивающегося вопросительным знаком 
        public ISet<string> FindSentencesFirstWords()
        {
            ISet<string> words = new HashSet<string>();
            foreach (Match m in Regex.Matches(this.Text, @"([A-ZА-Я][a-zа-я]*)[^.!?]*\?"))
                words.Add(m.Groups[1].Value);
            return words;
        }
        //подсчёта первых символов слова
        public IDictionary<string, int> FirstLetterCounts()
        {
            SortedDictionary<string, int> counts = new SortedDictionary<string, int>();
            Regex r = new Regex(@"\s([a-zA-Zа-яА-я])");
            foreach (Match m in r.Matches(this.Text))
            {
                string b = m.Groups[1].Value.ToUpper();
                if (counts.ContainsKey(b))
                    counts[b]++;
                else
                    counts[b] = 1; // при чтении было бы исключение «ключ не найден»                
            }
            return counts;
        }
        public IDictionary<string, int> DopZadanie()
        {
            IDictionary<string, int> counts = new Dictionary<string, int>();
            Regex r = new Regex(@"(?<=[^.!?\n\s]\s)[A-ZА-Я][A-Za-zА-Яа-я]+");
            foreach (Match m in r.Matches(this.Text))
            {
                string b = m.Groups[0].Value;
                if (counts.ContainsKey(b))
                    counts[b]++;
                else
                    counts[b] = 1; // при чтении было бы исключение «ключ не найден»                
            }
            return counts;
        }
    }
}
