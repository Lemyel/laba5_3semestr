﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba5_3semestr
{
    public partial class StatisticsForm : Form
    {
        public StatisticsForm()
        {
            InitializeComponent();
        }

        public StatisticsForm(IDictionary<string, int> stats, (string, string) columnsName)
        {
            InitializeComponent();
            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[0].HeaderText = columnsName.Item1;
            dataGridView1.Columns[1].HeaderText = columnsName.Item2;
            foreach (KeyValuePair<string, int> p in stats)
            {
                dataGridView1.Rows.Add(new object[] { p.Key, p.Value });
            }

        }


    }
}